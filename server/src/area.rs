use std::collections::HashSet;
use gotham::{
    helpers::http::response::create_response,
    hyper::{Body, Response},
    state::{FromState, State},
};
use mime::APPLICATION_JSON;
use http::StatusCode;
use crate::{AppState, PointExtractor};


pub fn get_details(state: State) -> (State, Response<Body>) {
    let pe = PointExtractor::borrow_from(&state);
    let app_state = AppState::borrow_from(&state);
    let mut seen_srcs = HashSet::new();
    let result = app_state.with_db(|db| {
        db.query("SELECT src, attrs FROM areas WHERE coords @> $1::point ORDER BY coords <-> $1::point ASC", &[
            &pe.to_point()
        ]).unwrap()
    }).into_iter().filter(|row| {
        let src: &str = row.get(0);
        if seen_srcs.contains(src) {
            false
        } else {
            seen_srcs.insert(src.to_owned());
            true
        }
    }).map(|row| {
        let attrs: serde_json::Value = row.get(1);
        attrs
    }).collect();

    let body = serde_json::to_string(&serde_json::Value::Array(result)).unwrap();
    let res = create_response(&state, StatusCode::OK, APPLICATION_JSON, body);
    (state, res)
}
