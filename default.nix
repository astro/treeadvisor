{ pkgs ? import <nixpkgs> {} }:

rec {
  geojsons = {
    # https://opendata.dresden.de/informationsportal/#app/mainpage//Stadtb%C3%A4ume
    trees = "https://kommisdd.dresden.de/net4/public/ogcapi/collections/L1261/items?limit=10000000";
    # Natürliche Funktionen des Bodens: https://kommisdd.dresden.de/net4/public/ogcapi/collections/L859
    boden_fkt = "https://kommisdd.dresden.de/net4/public/ogcapi/collections/L859/items?limit=10000000";
    # Gebietstypen des natürlichen Wasserhaushaltes: https://kommisdd.dresden.de/net4/public/ogcapi/collections/L1106
    wasserhaushalt = "https://kommisdd.dresden.de/net4/public/ogcapi/collections/L1106/items?limit=10000000";
    # Wasserspeichervermögen: https://kommisdd.dresden.de/net4/public/ogcapi/collections/L849
    wasserspeicher = "https://kommisdd.dresden.de/net4/public/ogcapi/collections/L849/items?limit=10000000";
    
  };

  # TODO:
  # im Grundwasser
  # UmweltWasser
  # Gebietstypen des natürlichen Wasserhaushaltes
  # UmweltWasser
  # Grundwasser - aktuelle Messwerte

  # Plants For A Future
  pfaf_sql = pkgs.requireFile {
    name = "PlantsForAFuture.sql";
    sha256 = "1wya1yb3hyybr71znmag7kq0jdc1cwazaqfp375jbfmaql3r92mq";
    message = "PlantsForAFuture.sql, compatible with PostgreSQL";
  };

  init-database = with pkgs; writeShellScript "populate-database" ''
    set -e

    ${lib.concatMapStrings (name: ''
      F=${name}.geojson
      [ -e $F ] || ${wget}/bin/wget -O $F "${geojsons.${name}}"
      ls -l $F
      # ${gdal}/bin/ogr2ogr -f "PostgreSQL" PG:"dbname=treeadvisor user=treeadvisor" -nln ${name} $F
    '') (builtins.attrNames geojsons)}

    ${postgresql}/bin/psql -f ${./db.sql} treeadvisor
    ${deno}/bin/deno run --allow-read --allow-net ${./import_geojson.ts} GEOENV boden_fkt.geojson wasserhaushalt.geojson wasserspeicher.geojson
    ${deno}/bin/deno run --allow-read --allow-net ${./import_geojson.ts} TREES trees.geojson

    ${postgresql}/bin/psql -f ${pfaf_sql} treeadvisor
  '';
}
