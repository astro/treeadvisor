{ pkgs, config, lib, ... }:

let
in
{
  config = {
    networking.hostName = "treeadvisor";

    users.users.treeadvisor = {
      isSystemUser = true;
      group = "treeadvisor";
    };
    users.groups.treeadvisor = {};

    services.postgresql = {
      enable = true;
      extraPlugins = with pkgs; [ postgis ];
      # TODO: tmp
      enableTCPIP = true;
      authentication = ''
        host    all             all            0.0.0.0/0            md5
      '';

      ensureDatabases = [ "treeadvisor" ];
      ensureUsers = [ {
        name = "treeadvisor";
        ensurePermissions = {
          "DATABASE treeadvisor" = "ALL PRIVILEGES";
        };
      } ];
    };

    systemd.tmpfiles.rules = [
      "d /var/lib/treeadvisor 0755 treeadvisor treeadvisor -"
    ];
    
    systemd.services.treeadvisor-db-init = {
      wantedBy = [ "multi-user.target" ];
      unitConfig.ConditionPathExists = [ "!/var/lib/treeadvisor/.db-init" ];
      serviceConfig = {
        User = "treeadvisor";
        Group = "treeadvisor";
      };
      environment.HOME = "/tmp";
      script = ''
        cd /var/lib/treeadvisor

        ${(import ./. { inherit pkgs; }).init-database}

        touch /var/lib/treeadvisor/.db-init
      '';
    };
  };
}
